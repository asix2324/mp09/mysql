<?php
        include_once( 'mysql_func.php' );
        include_once( 'utils.php' );

        $loadData = array(
            'users' => array(
                'leomessi' => array(
                    'password'  => '123',
                    'salt'      => getPasswordSalt()
                ),
                'rafanadal' => array(
                    'password'  => '123',
                    'salt'      => getPasswordSalt()
                ),
                'etoo' => array(
                    'password'  => '123',
                    'salt'      => getPasswordSalt()
                ),
                'obama' => array(
                    'password'  => '123',
                    'salt'      => getPasswordSalt()
                ),
                'pauperis' => array(
                    'password'  => '123456',
                    'salt'      => getPasswordSalt()
                ),
                'pep' => array(
                    'password'  => '1234',
                    'salt'      => getPasswordSalt()
                )
            ),
            'bookings' => array(
                array(
                    'checking' => new \Datetime( 'tomorrow' ),
                    'checkout' => new \Datetime( '+3 days' )
                ),
                array(
                    'checking' => new \Datetime( '+1 week' ),
                    'checkout' => new \Datetime( '+2 weeks' )
                ),
                array(
                    'checking' => new \Datetime( '+5 week' ),
                    'checkout' => new \Datetime( '+9 weeks' )
                )
            )
        );
        $now    = new \Datetime( 'now' );

        $mysql  = new \MySQL_Func();
	$sql    = array(
            'createDatabase' => array(
                sprintf( 'DROP DATABASE `%s`;',     $mysql->getDBName() ),
                sprintf( 'CREATE DATABASE `%s`;',   $mysql->getDBName() )
            ),
            'createSchema' => array(
                'CREATE TABLE bookings (id INT AUTO_INCREMENT NOT NULL, client_id INT NOT NULL, checking_date DATETIME NOT NULL, checkout_date DATETIME NOT NULL, executed INT(1) NOT NULL, room_number INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, timezone VARCHAR(30) NOT NULL, INDEX IDX_7A853C35A76ED395 (client_id), INDEX checking_date_idx (checking_date), INDEX checkout_date_idx (checkout_date), UNIQUE INDEX room_number_checkout_uniq (room_number, checkout_date), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;',
                'CREATE TABLE bookings_services (id INT AUTO_INCREMENT NOT NULL, booking_id INT NOT NULL, service_id INT NOT NULL, INDEX IDX_9BA18C543301C60 (booking_id), INDEX IDX_9BA18C54ED5CA9E6 (service_id), UNIQUE INDEX user_service_uniq (booking_id, service_id ), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;',
                'CREATE TABLE roles (id TINYINT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, timezone VARCHAR(30) NOT NULL, UNIQUE INDEX UNIQ_B63E2EC75E237E06 (name), INDEX name_idx (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;',
                'CREATE TABLE clients (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, email VARCHAR(100) NOT NULL, address VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, timezone VARCHAR(30) NOT NULL, UNIQUE INDEX UNIQ_C82E74E7927C74 (email), INDEX IDX_C82E74A76ED395 (user_id), INDEX email_idx (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;',
                'CREATE TABLE services (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, description VARCHAR(255) NOT NULL, pvp NUMERIC(15, 2) NOT NULL, file_path VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, timezone VARCHAR(30) NOT NULL, UNIQUE INDEX UNIQ_7332E1695E237E06 (name), INDEX name_idx (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;',
                'CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, role_id TINYINT NOT NULL, username VARCHAR(50) NOT NULL, password VARCHAR(100) NOT NULL, salt VARCHAR(255) NOT NULL, nif VARCHAR(25) NOT NULL, name VARCHAR(25) NOT NULL, surname1 VARCHAR(25) NOT NULL, surname2 VARCHAR(25) NOT NULL, phone VARCHAR(25) NOT NULL, enabled INT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, timezone VARCHAR(30) NOT NULL, UNIQUE INDEX UNIQ_1483A5E9BF396750 (id), UNIQUE INDEX UNIQ_1483A5E9F85E0677 (username), UNIQUE INDEX UNIQ_1483A5E9ADE62BBB (nif), UNIQUE INDEX UNIQ_1483A5E9444F97DD (phone), INDEX IDX_1483A5E9D60322AC (role_id), INDEX username_idx (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;',
                'ALTER TABLE bookings ADD CONSTRAINT FK_7A853C35A76ED395 FOREIGN KEY (client_id) REFERENCES clients (id) ON DELETE CASCADE;',
                'ALTER TABLE bookings_services ADD CONSTRAINT FK_9BA18C543301C60 FOREIGN KEY (booking_id) REFERENCES bookings (id) ON DELETE CASCADE;',
                'ALTER TABLE bookings_services ADD CONSTRAINT FK_9BA18C54ED5CA9E6 FOREIGN KEY (service_id) REFERENCES services (id);',
                'ALTER TABLE clients ADD CONSTRAINT FK_C82E74A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE;',
                'ALTER TABLE users ADD CONSTRAINT FK_1483A5E9D60322AC FOREIGN KEY (role_id) REFERENCES roles (id) ON DELETE CASCADE;'
            ),
            'loadData' => array(
                "INSERT INTO roles (name, created_at, updated_at, timezone) VALUES ('client', '2015-11-17 12:52:26', '2015-11-17 12:52:26', 'Europe/Madrid');",
                "INSERT INTO roles (name, created_at, updated_at, timezone) VALUES ('worker', '2015-11-17 12:52:26', '2015-11-17 12:52:26', 'Europe/Madrid');",
                "INSERT INTO roles (name, created_at, updated_at, timezone) VALUES ('administrator', '2015-11-17 12:52:26', '2015-11-17 12:52:26', 'Europe/Madrid');",
                "INSERT INTO services (name, description, pvp, file_path, created_at, updated_at, timezone) VALUES ('Trekking', 'Backpacking (trekking) is also popular in the Himalayas, where porters and pack animals are often used. Typical trekking regions in Nepal are Annapurna, Dolpo, Langtang, Manaslu, Kangchenjunga, Mount Everest. In India the Kashmir Valley also has many trekking routes.', '69,95', 'foobar', '2015-11-17 12:52:26', '2015-11-17 12:52:26', 'Europe/Madrid');",
                "INSERT INTO services (name, description, pvp, file_path, created_at, updated_at, timezone) VALUES ('Jacuzzi', 'Moreover, every bath, shower system, and home spa accessory is backed by our extensive warranty and nationwide service network.', '35,95', 'foo', '2015-11-17 12:52:26', '2015-11-17 12:52:26', 'Europe/Madrid');",
                "INSERT INTO services (name, description, pvp, file_path, created_at, updated_at, timezone) VALUES ('Tennis', 'Play a relaxed tennis match against Rafa Nadal.', '285,95', 'bar', '2015-11-17 12:52:26', '2015-11-17 12:52:26', 'Europe/Madrid');",

                sprintf( "INSERT INTO users (username, password, salt, nif, name, surname1, surname2, phone, enabled, created_at, updated_at, timezone, role_id)"
                        . "VALUES ('leomessi', '%1\$s', '%2\$s', '12345678Z', 'Lionel', 'Andrés', 'Messi', '669685854', 1, '%3\$s', '%\3\$s', 'Europe/Madrid', 1);"
                        ,
                        encodePassword( $loadData[ 'users' ][ 'leomessi' ][ 'password' ], $loadData[ 'users' ][ 'leomessi' ][ 'salt' ] ),
                        $loadData[ 'users' ][ 'leomessi' ][ 'salt' ],
                        $now->format( 'Y-m-d H:i:s' )
                ),

                sprintf( "INSERT INTO users (username, password, salt, nif, name, surname1, surname2, phone, enabled, created_at, updated_at, timezone, role_id) "
                        . "VALUES ('pauperis', '%1\$s', '%2\$s', '12345670Z', 'Pau', 'Peris', 'Rodriguez', '669685851', 1, '%3\s', '%3\s', 'Europe/Madrid', 3);"
                        ,
                        encodePassword( $loadData[ 'users' ][ 'pauperis' ][ 'password' ], $loadData[ 'users' ][ 'pauperis' ][ 'salt' ] ),
                        $loadData[ 'users' ][ 'pauperis' ][ 'salt' ],
                        $now->format( 'Y-m-d H:i:s' )
                ),

                sprintf( "INSERT INTO users (username, password, salt, nif, name, surname1, surname2, phone, enabled, created_at, updated_at, timezone, role_id) "
                        . "VALUES ('pep', '%1\$s', '%2\$s', '12345679Z', 'Josep', 'Guardiola', 'Sala', '669685852', 1, '%3\s', '%3\s', 'Europe/Madrid', 2);"
                        ,
                        encodePassword( $loadData[ 'users' ][ 'pep' ][ 'password' ], $loadData[ 'users' ][ 'pep' ][ 'salt' ] ),
                        $loadData[ 'users' ][ 'pep' ][ 'salt' ],
                        $now->format( 'Y-m-d H:i:s' )
                ),

                sprintf( "INSERT INTO users (username, password, salt, nif, name, surname1, surname2, phone, enabled, created_at, updated_at, timezone, role_id) "
                        . "VALUES ('rafa', '%1\$s', '%2\$s', '12345671Z', 'Rafael', 'Nadal', 'Parera', '669685858', 1, '%3\s', '%3\s', 'Europe/Madrid', 1);"
                        ,
                        encodePassword( $loadData[ 'users' ][ 'rafanadal' ][ 'password' ], $loadData[ 'users' ][ 'rafanadal' ][ 'salt' ] ),
                        $loadData[ 'users' ][ 'rafanadal' ][ 'salt' ],
                        $now->format( 'Y-m-d H:i:s' )
                ),

                sprintf( "INSERT INTO users (username, password, salt, nif, name, surname1, surname2, phone, enabled, created_at, updated_at, timezone, role_id) "
                        . "VALUES ('etoo', '%1\$s', '%2\$s', '12345677Z', 'Samuel', 'Eto\'o', 'Fils', '669685857', 1, '%3\s', '%3\s', 'Europe/Madrid', 1);"
                        ,
                        encodePassword( $loadData[ 'users' ][ 'etoo' ][ 'password' ], $loadData[ 'users' ][ 'etoo' ][ 'salt' ] ),
                        $loadData[ 'users' ][ 'etoo' ][ 'salt' ],
                        $now->format( 'Y-m-d H:i:s' )
                ),

                sprintf( "INSERT INTO users (username, password, salt, nif, name, surname1, surname2, phone, enabled, created_at, updated_at, timezone, role_id) "
                        . "VALUES ('obama', '%1\$s', '%2\$s', '12345676Z', 'Barack', 'Hussein', 'Obama II', '669685856', 1, '%3\s', '%3\s', 'Europe/Madrid', 1);"
                        ,
                        encodePassword( $loadData[ 'users' ][ 'obama' ][ 'password' ], $loadData[ 'users' ][ 'obama' ][ 'salt' ] ),
                        $loadData[ 'users' ][ 'obama' ][ 'salt' ],
                        $now->format( 'Y-m-d H:i:s' )
                ),

                "INSERT INTO clients (email, address, created_at, updated_at, timezone, user_id) VALUES ('leomessi@gmail.com', 'Balmes 211, Barcelona', '2015-11-17 12:52:26', '2015-11-17 12:52:26', 'Europe/Madrid', 1)",
                "INSERT INTO clients (email, address, created_at, updated_at, timezone, user_id) VALUES ('rafanadal@gmail.com', 'Manacor 211, Barcelona', '2015-11-17 12:52:26', '2015-11-17 12:52:26', 'Europe/Madrid', 4);",
                "INSERT INTO clients (email, address, created_at, updated_at, timezone, user_id) VALUES ('etoo@gmail.com', 'Camerun 211, Barcelona', '2015-11-17 12:52:26', '2015-11-17 12:52:26', 'Europe/Madrid', 5);",
                "INSERT INTO clients (email, address, created_at, updated_at, timezone, user_id) VALUES ('obama@gmail.com', 'Honolulu 211, Barcelona', '2015-11-17 12:52:26', '2015-11-17 12:52:26', 'Europe/Madrid', 6);",
                sprintf( "INSERT INTO bookings (checking_date, checkout_date, executed, room_number, created_at, updated_at, timezone, client_id)"
                        . "VALUES ('2015-11-17 12:52:26', '2015-12-01 12:52:26', 1, '21', '2015-11-17 12:52:26', '2015-11-17 12:52:26', 'Europe/Madrid', 1);"
                        ,
                        $loadData[ 'bookings' ][ 0 ][ 'checking' ]->format( 'Y-m-d H:i:s' ),
                        $loadData[ 'bookings' ][ 0 ][ 'checkout' ]->format( 'Y-m-d H:i:s' )
                ),
                sprintf( "INSERT INTO bookings (checking_date, checkout_date, executed, room_number, created_at, updated_at, timezone, client_id)"
                        . "VALUES ('%s', '%s', 1, '26', '2015-11-17 12:52:26', '2015-11-17 12:52:26', 'Europe/Madrid', 1);"
                        ,
                        $loadData[ 'bookings' ][ 1 ][ 'checking' ]->format( 'Y-m-d H:i:s' ),
                        $loadData[ 'bookings' ][ 1 ][ 'checkout' ]->format( 'Y-m-d H:i:s' )
                ),
                sprintf( "INSERT INTO bookings (checking_date, checkout_date, executed, room_number, created_at, updated_at, timezone, client_id)"
                        . "VALUES ('%s', '%s', 0, '69', '2015-11-17 12:52:26', '2015-11-17 12:52:26', 'Europe/Madrid', 2);"
                        ,
                        $loadData[ 'bookings' ][ 2 ][ 'checking' ]->format( 'Y-m-d H:i:s' ),
                        $loadData[ 'bookings' ][ 2 ][ 'checkout' ]->format( 'Y-m-d H:i:s' )
                ),
                "INSERT INTO bookings_services (booking_id, service_id) VALUES (1, 1);",
                "INSERT INTO bookings_services (booking_id, service_id) VALUES (1, 3);",
                "INSERT INTO bookings_services (booking_id, service_id) VALUES (2, 1);",
                "INSERT INTO bookings_services (booking_id, service_id) VALUES (2, 3);",
                "INSERT INTO bookings_services (booking_id, service_id) VALUES (2, 2);"
            )
	);

        $mysql->setDSN( sprintf( 'mysql:host=%s', $mysql->getHost() ) );
        foreach( $sql[ 'createDatabase' ] as $sqlQuery ) {
            $mysql->connect()->query( $sqlQuery );
        }

        $mysql->setDSN( sprintf( 'mysql:host=%s;dbname=%s', $mysql->getHost(), $mysql->getDBName() ) );
        foreach( $sql[ 'createSchema' ] as $sqlQuery ) {
            $mysql->connect()->query( $sqlQuery );
        }

        foreach( $sql[ 'loadData' ] as $sqlQuery ) {
            $mysql->connect()->query( $sqlQuery );
        }
?>
