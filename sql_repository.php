<?php
        /* En aquest fixer coloquem totes les consultes SQL. */
        //Antigament quan obteniem dades a través d'inputs de formularis i les utilitzavem per realitzar consultes SQL
        //les haviem d'escapar i tractar per assegurar-nos que no patiem SQL injection. Avui dia gràcies a MYSQL_PDO, ja no cal.

        include_once( 'mysql_func.php' );

        function getUserByUsername_SQL( $username ) {

            $sql = '
                    SELECT
                        u.username,
                        u.password,
                        u.salt,
                        r.name AS role
                    FROM
                        `users` AS u
                    INNER JOIN
                        `roles` AS r
                        ON r.id = u.role_id
                    WHERE
                        `u`.`username` = :username
                        AND `u`.enabled IS TRUE
                    ';

            $mysql  = new \MySQL_Func();
            $mysql->connect();
            //Siem de realitzar consultes SQL amb dades que provenen del client,
            //utilitzarem el sistema prepare()+bindValue() ja que d'aquesta manera
            //PDO_SQL tracta la consulta $sql per un costat i els paràmetres bindValue()
            //per un altre, prevenint així SQL injection.
            $stmt = $mysql->getDBH()->prepare( $sql );
            //Aquest tercer paràmetre és opcional però important per ajudar a prevenir atacs.
            //Li estem dient a PDO_MYSQL que comprovi que aquest paràmetre que rebem per formulari
            //sigui un input.
            $stmt->bindValue( 'username', $username, \PDO::PARAM_STR  );
            $stmt->execute();

            $r = $stmt->fetchAll( \PDO::FETCH_ASSOC );
            if( !empty( $r ) ) { $r = array_shift( $r ); }
            else { $r = array(); }

            return $r;
        }

        function getUsernameById_SQL( $userId ) {

            $sql = '
                    SELECT
                        u.username
                    FROM
                        `users` AS u
                    INNER JOIN
                        `roles` AS r
                        ON r.id = u.role_id
                    WHERE
                        `u`.`id` = :userId
                    ';

            $mysql  = new \MySQL_Func();
            $mysql->connect();
            //Siem de realitzar consultes SQL amb dades que provenen del client,
            //utilitzarem el sistema prepare()+bindValue() ja que d'aquesta manera
            //PDO_SQL tracta la consulta $sql per un costat i els paràmetres bindValue()
            //per un altre, prevenint així SQL injection.
            $stmt = $mysql->getDBH()->prepare( $sql );
            //Aquest tercer paràmetre és opcional però important per ajudar a prevenir atacs.
            //Li estem dient a PDO_MYSQL que comprovi que aquest paràmetre que rebem per formulari
            //sigui un input.
            $stmt->bindValue( 'userId', $userId, \PDO::PARAM_INT  );
            $stmt->execute();

            $r = $stmt->fetchAll( \PDO::FETCH_ASSOC );
            if( !empty( $r ) ) { $r = array_shift( $r ); }
            else { $r = array(); }

            return $r[ 'username' ];
        }

        function deleteUserById_SQL( $userId ) {
            $sql = sprintf( '
                    DELETE
                    FROM
                        `users`
                    WHERE
                        `id` = %s
                    ', $userId );
            $mysql  = new \MySQL_Func();
            return $mysql->connect()->getDBH()->exec( $sql );
        }

        function getUsersByRol_SQL( $roles ) {

            $sql = '
                    SELECT
                        u.id,
                        u.username,
                        r.name AS role,
                        u.name,
                        u.surname1,
                        u.surname2,
                        u.nif,
                        u.phone,
                        u.enabled,
                        u.created_at,
                        u.updated_at
                    FROM
                        `users` AS u
                    INNER JOIN
                        `roles` AS r
                        ON r.id = u.role_id
                    WHERE
                        `r`.`name` IN ( :role0, :role1 )
                    ';

            $mysql  = new \MySQL_Func();
            $mysql->connect();
            $stmt = $mysql->getDBH()->prepare( $sql );
            $stmt->bindValue( 'role0', array_shift( $roles ), \PDO::PARAM_STR  );
            $stmt->bindValue( 'role1', array_shift( $roles ), \PDO::PARAM_STR  );
            $stmt->execute();

            $usersArr = array();
            while( ( $c = $stmt->fetch( \PDO::FETCH_ASSOC ) ) !== false )
            {
                if( empty( $usersArr ) ) {
                    $usersArr = array(
                        $c[ 'id' ] => $c
                    );
                } else {
                    $usersArr[ $c[ 'id' ] ] = $c;
                }

                $eId = encryptToken( $c[ 'id' ], true );
                $url = sprintf( 'http://%s/%sedit_user.php?u=%s', $_SERVER['SERVER_ADDR'], getBaseURI(), $eId );
                $usersArr[ $c[ 'id' ] ][ 'edit' ] = $url;
            }

            if( empty( $usersArr ) ) {
                $usersArr = array(
                    array(
                        'id'        => null,
                        'username'  => null,
                        'role'      => null,
                        'name'      => null,
                        'surname1'  => null,
                        'surname2'  => null,
                        'nif'       => null,
                        'phone'     => null,
                        'enabled'   => null,
                        'created_at'=> null,
                        'updated_at'=> null,
                        'edit'      => null
                    )
                );
            }

            return $usersArr;
        }

        function getAllClients_SQL() {

            $sql = '
                    SELECT
                        u.id,
                        u.username,
                        r.name AS role,
                        c.email,
                        u.name,
                        u.surname1,
                        u.surname2,
                        u.nif,
                        u.phone,
                        c.address,
                        u.enabled,
                        u.created_at,
                        u.updated_at
                    FROM
                        `users` AS u
                    INNER JOIN
                        `roles` AS r
                        ON r.id = u.role_id
                    INNER JOIN
                        `clients` AS c
                        ON u.id = c.user_id
                    WHERE
                        `r`.`name` = \'client\'
                    ';

            $mysql  = new \MySQL_Func();
            $stmt = $mysql->connect()->getDBH()->query( $sql );

            $usersArr = array();
            while( ( $c = $stmt->fetch( \PDO::FETCH_ASSOC ) ) !== false )
            {
                if( empty( $usersArr ) ) {
                    $usersArr = array(
                        $c[ 'id' ] => $c
                    );
                } else {
                    $usersArr[ $c[ 'id' ] ] = $c;
                }

                $eId = encryptToken( $c[ 'id' ], true );
                $url = sprintf( 'http://%s/%sshow_client.php?u=%s', $_SERVER['SERVER_ADDR'], getBaseURI(), $eId );
                $usersArr[ $c[ 'id' ] ][ 'show' ] = $url;

                $url = sprintf( 'http://%s/%sedit_client.php?u=%s', $_SERVER['SERVER_ADDR'], getBaseURI(), $eId );
                $usersArr[ $c[ 'id' ] ][ 'edit' ] = $url;

                $url = sprintf( 'http://%s/%sconfirm_delete_user.php?u=%s', $_SERVER['SERVER_ADDR'], getBaseURI(), $eId );
                $usersArr[ $c[ 'id' ] ][ 'delete' ] = $url;
            }

            if( empty( $usersArr ) ) {
                $usersArr = array(
                    array(
                        'id'        => null,
                        'username'  => null,
                        'role'      => null,
                        'email'     => null,
                        'name'      => null,
                        'surname1'  => null,
                        'surname2'  => null,
                        'nif'       => null,
                        'phone'     => null,
                        'address'   => null,
                        'enabled'   => null,
                        'created_at'=> null,
                        'updated_at'=> null,
                        'show'      => null,
                        'edit'      => null
                    )
                );
            }

            return $usersArr;
        }

        function getUserDetails_SQL( $userId ) {

            $sql = '
                    SELECT
                        `u`.`id`,
                        `u`.`username`,
                        `r`.`name` AS `role`,
                        `c`.`id` AS `cId`,
                        `c`.`email`,
                        `u`.`name`,
                        `u`.`surname1`,
                        `u`.`surname2`,
                        `u`.`nif`,
                        `u`.`phone`,
                        `c`.`address`,
                        `u`.`enabled`,
                        `u`.`created_at`,
                        `u`.`updated_at`,
                        `b`.`id` AS `bId`,
                        `b`.`checking_date`,
                        `b`.`checkout_date`,
                        `b`.`executed`,
                        `b`.`room_number`,
                        `b`.`created_at` AS `b_created_at`,
                        `b`.`updated_at` AS `b_updated_at`,
                        `bs`.`id` AS `bsId`,
                        `s`.`name` AS `serviceName`,
                        `s`.`description`
                    FROM
                        `users` AS `u`
                    INNER JOIN
                        `roles` AS `r`
                        ON `r`.`id` = `u`.`role_id`
                    INNER JOIN
                        `clients` AS `c`
                        ON `u`.`id` = `c`.`user_id`
                    LEFT JOIN (
                        `bookings` AS `b`
                        LEFT JOIN (
                            `bookings_services` AS `bs`
                            INNER JOIN
                                `services` AS `s`
                                ON `s`.`id` = `bs`.`service_id`
                        ) ON `bs`.`booking_id` = `b`.`id`
                    ) ON `c`.`id` = `b`.`client_id`
                    WHERE
                        `u`.`id` = :userId
                    ';

            $mysql  = new \MySQL_Func();
            $mysql->connect();
            $stmt = $mysql->getDBH()->prepare( $sql );
            $stmt->bindValue( 'userId', $userId, \PDO::PARAM_INT );
            $stmt->execute();

            $userArr = array();
            while( ( $c = $stmt->fetch( \PDO::FETCH_ASSOC ) ) !== false )
            {
                $eId            = encryptToken( $c[ 'id' ], true );
                $urlEditUser    = sprintf( 'http://%s/%sedit_client.php?u=%s', $_SERVER['SERVER_ADDR'], getBaseURI(), $eId );
                //Primera iteració, si l'array està buit creem l'estructura d'usuari
                if( empty( $userArr ) ) {
                    $userArr = array(
                        $c[ 'id' ] => array(
                            'id'            => $c[ 'id' ],
                            'username'      => $c[ 'username' ],
                            'role'          => $c[ 'role' ],
                            'name'          => $c[ 'name' ],
                            'surname1'      => $c[ 'surname1' ],
                            'surname2'      => $c[ 'surname2' ],
                            'cId'           => $c[ 'cId' ],
                            'email'         => $c[ 'email' ],
                            'nif'           => $c[ 'nif' ],
                            'phone'         => $c[ 'phone' ],
                            'address'       => $c[ 'address' ],
                            'enabled'       => $c[ 'enabled' ],
                            'created_at'    => $c[ 'created_at' ],
                            'updated_at'    => $c[ 'updated_at' ],
                            'edit'          => $urlEditUser,
                            'bookings'      => array()
                        )
                    );
                }
                //Si l'usuari té bookings i no estan afegits, els afegim
                if( !empty( $c[ 'bId' ] ) && empty( $userArr[ $c[ 'id' ] ][ 'bookings' ][ $c[ 'bId' ] ] ) ) {
                    $userArr[ $c[ 'id' ] ][ 'bookings' ][ $c[ 'bId' ] ] = array(
                        'room_number'   => $c[ 'room_number' ],
                        'executed'      => $c[ 'executed' ],
                        'checkingDate'  => $c[ 'checking_date' ],
                        'checkoutDate'  => $c[ 'checkout_date' ],
                        'created_at'    => $c[ 'b_created_at' ],
                        'updated_at'    => $c[ 'b_updated_at' ],
                        'edit'          => sprintf( 'http://%s/%sedit_booking.php?b=%s', $_SERVER['SERVER_ADDR'], getBaseURI(), encryptToken( $c[ 'bId' ], true ) ),
                        'services'      => array()
                    );
                }
                //Si l'usuari té mes serveis dins el mateix booking i encara no els hem afegit, els afegim
                if( !empty( $c[ 'bsId' ] ) && empty( $userArr[ $c[ 'id' ] ][ 'bookings' ][ $c[ 'bId' ] ][ 'services' ][ $c[ 'bsId' ] ] ) ) {
                    $userArr[ $c[ 'id' ] ][ 'bookings' ][ $c[ 'bId' ] ][ 'services' ][ $c[ 'bsId' ] ] = array(
                        'name'          => $c[ 'serviceName' ],
                        'description'   => $c[ 'description' ],
                        'edit'          => sprintf( 'http://%s/%sedit_service.php?b=%s', $_SERVER['SERVER_ADDR'], getBaseURI(), encryptToken( $c[ 'bsId' ], true ) ),
                    );
                }
            }

            return $userArr;
        }
?>
