<?php

    include_once( 'sql_repository.php' );
    include_once( 'utils.php' );

    return call_user_func( $_GET[ '_meth' ], array( 'u' => $_GET[ 'u' ], 'form' => $_POST ) );

    /*
     * Cal ajustar el codi per el cas que es canvii el rol a través del formulari.
     * Aquest cas s'hauria de tractar a part.
     */
    function updateClient( $params ) {

        $r = grantAccessOrLogout();
        //Si l'usuari no té permisos aturem l'execució del codi
        if( $r !== true ) { return header(); }
        //Si no es tracta d'un POST reenviem l'usuari a l'edició del formulari
        if( empty( $_POST ) ) {
            $uri = str_replace( 'client_controller.php', 'edit_client.php', $_SERVER[ 'REQUEST_URI' ] );
            return header( sprintf( 'Location: http://%s/%s', $_SERVER['SERVER_ADDR'], $uri ) );
            
        }

        $notice = array(
            'text'  => 'No s\'han fet canvis al formulari.',
            'color' => 'red'
        );

        extract( $params );

        $u      = decryptToken( $u );
        $user   = getUserDetails_SQL( $u );
        $user   = array_shift( $user );
        $newUSer= getArrayDiff( $form[ 'client' ], $user );

        //Unicament executem el següent codi si han realitzat canvis al formulari
        if( !empty( $newUSer ) ) {

            //Reestructurem l'array $form[ 'client' ] per mantenir una estructura similar
            //a la bbdd
            $clientArr  = array( 'email', 'address' );
            $client     = array();
            foreach( $newUSer as $k => $v ) {
                if( in_array( $k, $clientArr ) ) {
                    $client[ $k ] = $v;
                    unset( $newUSer[ $k ] );
                }
            }
            if( !empty( $client ) ) { $newUSer[ 'clients' ][ $user[ 'cId' ] ] = $client; }

            $data   = array( 'users' => array( $u => $newUSer ) );
            $sqlArr = getSQLQueries( 'update', $data );

            if( !empty( $sqlArr ) ) {                
                try {
                    $mysql  = new \MySQL_Func();
                    $dbh    = $mysql->connect()->getDBH();
                    $dbh->beginTransaction();
                    //Executem el codi SQL a través d'un 'closure'
                    $execQuery = function( $query ) use ( $dbh ) {
                        return $dbh->exec( $query );
                    };
                    //Recorrem recursivament l'array subministrant
                    //cadascun dels values al closure anterior
                    array_walk_recursive( $sqlArr , $execQuery );
                    $dbh->commit();

                    $notice = array(
                        'text'  => 'Els canvis s\'han guardat correctament.',
                        'color' => 'green'
                    );
                } catch ( \Exception $e ) {
                    $dbh->rollBack();
                    throw new \Exception( sprintf( 'PDO_MYSQL Error trying to run SQL: %s', $e->getMessage() ) );
                }
            }
        }

        include_once( 'sql_controller.php' );
        $tpl = getClientDetails( $u );
        $tpl[ 'form' ][ 'notice' ] = $notice;

        return _include( 'edit_client_tpl.php', array( 'array' => $tpl ), true );
    }
?>
