<?php
    /**
     * Simple and work in progress class which tries to abstract and simply running queries against MySQL.
     * 
     * @author Pau Peris Rodriguez <pau@pauperis.com>
     */
	Class MySQL_Func
	{
        /**
        * Value property. Stores the driver used to connect to MySQL.
        *
        * @var string
        */
		protected $driver;

        /**
        * Value property. Stores the Host where MySQL backedn resides.
        *
        * @var string
        */
		protected $host;

		protected $port;
		protected $dbName;
		protected $user;
		protected $password;
		protected $dsn;
		protected $dbh;

        /**
        * Value property. Stores the Host where MySQL backedn resides.
        *
        * @var array
        */
		protected $options = array();

        /**
        * Constructor
        *
        * @param bool $utf8 Defines the charset used while exchanging data with MySQL.
        * @return void
        */
		public function __construct( $utf8 = true ) {
            include( 'config.php' );

			$this->driver   = $config[ 'mysql' ][ 'database_driver' ];
			$this->host     = $config[ 'mysql' ][ 'database_host' ];
			$this->port     = $config[ 'mysql' ][ 'database_port' ];
			$this->dbName   = $config[ 'mysql' ][ 'database_name' ];
			$this->user     = $config[ 'mysql' ][ 'database_user' ];
			$this->password = $config[ 'mysql' ][ 'database_password' ];

            $this->dsn      = sprintf( 'mysql:host=%s;dbname=%s', $this->host, $this->dbName );

            if( $utf8 ) {
                //Setting the connection character set to UTF-8
                $this->options = array(
                    \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8'
                ); 
            }
		}

        /**
        * Gets the driver used to connect to MySQL.
        *
        * @return MySQL_Func
        */
        public function getDriver() {
            return $this->driver;
        }

        public function getHost() {
            return $this->host;
        }

        public function getPort() {
            return $this->port;
        }

        public function getDBName() {
            return $this->dbName;
        }

        public function getUser() {
            return $this->user;
        }

        public function getPassword() {
            return $this->password;
        }

        public function getDBH() {
            return $this->dbh;
        }

        public function setDSN( $dsn ) {
            $this->dsn = $dsn;
            return $this;
        }

        public function connect() {
            try {
                $this->dbh = new \PDO( $this->dsn, $this->user, $this->password, $this->options );
            } catch ( \PDOException $e ) {
                throw new \Exception( sprintf( 'Connection failed: %s', $e->getMessage() ) );
            }

            return $this;
        }

        public function query( $sql, $fetch = \PDO::FETCH_ASSOC ) {
            try {
                $statement = $this->dbh->query( $sql );
                if( !empty( $statement) ) { $row = $statement->fetch( $fetch ); }
                else { $row = false; }
            } catch ( \PDOException $e ) {
                throw new \Exception( sprintf( 'Query failed: ', $e->getMessage() ) );
            }

            return $row;
        }
	}
?>
