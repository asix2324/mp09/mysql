<?php
	$config = array( 
            'mysql' => array(
                    'database_driver'	=> 'pdo_mysql',
                    'database_host'     => '127.0.0.1',
                    'database_port'     => 3306,
                    'database_name'     => 'daw2024_m9_uf1_ac1',
                    'database_user'     => 'root',
                    'database_password' => null
            )
        );
?>
